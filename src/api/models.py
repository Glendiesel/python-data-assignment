from src.api import db


class Prediction(db.Model):
    id = db.Column('prediction_id', db.Integer, primary_key=True)
    feature11 = db.Column(db.Float())
    feature13 = db.Column(db.Float())
    feature15 = db.Column(db.Float())
    amount = db.Column(db.Float())
    result = db.Column(db.String(50))

    def __init__(self, feature11, feature13, feature15, amount, result):
        self.feature11 = feature11
        self.feature13 = feature13
        self.feature15 = feature15
        self.amount = amount
        self.result = result